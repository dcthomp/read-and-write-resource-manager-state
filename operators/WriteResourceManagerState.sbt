<?xml version="1.0" encoding="utf-8" ?>
<!-- Description of the model "WriteResourceManagerState" Operation -->
<SMTK_AttributeResource Version="3">

  <Definitions>
    <!-- Operation -->
    <include href="smtk/operation/Operation.xml"/>
    <AttDef Type="write resource manager state" Label="Write Resource Manager State" BaseType="operation">
      <BriefDecscription>
        Serialize all managed resources to a directory.
      </BriefDecscription>
      <ItemDefinitions>
        <Directory Name="directory" Label="Resource Manager Directory" ShouldExist="false">
          <BriefDescription>The name of the directory to which all managed resources should be written.</BriefDescription>
        </Directory>
      </ItemDefinitions>
    </AttDef>

    <!-- Result -->
    <include href="smtk/operation/Result.xml"/>
    <AttDef Type="result(write resource manager state)" BaseType="result">
      <ItemDefinitions>
        <Resource Name="resource" NumberOfRequiredValues="0"
                  Extensible="true" HoldReference="true"></Resource>
      </ItemDefinitions>
    </AttDef>
  </Definitions>

</SMTK_AttributeResource>
