//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#ifndef smtk_resource_manager_state_WriteResourceManagerState_h
#define smtk_resource_manager_state_WriteResourceManagerState_h

#include "operators/Exports.h"

#include "smtk/operation/XMLOperation.h"

namespace smtk
{
namespace resource_manager_state
{

/// Write the state of the resource manager.
class SMTKREADWRITERESOURCEMANAGERSTATE_EXPORT WriteResourceManagerState :
    public smtk::operation::XMLOperation
{
public:
  smtkTypeMacro(smtk::resource_manager_state::WriteResourceManagerState);
  smtkSharedPtrCreateMacro(smtk::operation::Operation);
  smtkSuperclassMacro(smtk::operation::XMLOperation);

  virtual bool ableToOperate() override;

protected:
  WriteResourceManagerState();

  Result operateInternal() override;
  virtual const char* xmlDescription() const override;
  void markModifiedResources(Result&) override;
};
}
}

#endif
