//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#include "operators/WriteResourceManagerState.h"

#include "operators/WriteResourceManagerState_xml.h"

#include "smtk/attribute/Attribute.h"
#include "smtk/attribute/Definition.h"
#include "smtk/attribute/DirectoryItem.h"
#include "smtk/attribute/FileItem.h"
#include "smtk/attribute/IntItem.h"
#include "smtk/attribute/ResourceItem.h"

#include "smtk/common/Paths.h"

#include "smtk/io/Logger.h"

#include "smtk/resource/Manager.h"
#include "smtk/resource/json/jsonResource.h"

#include "smtk/operation/Manager.h"
#include "smtk/operation/operators/WriteResource.h"

#include "smtk/common/Paths.h"

#include <fstream>

namespace smtk
{
namespace resource_manager_state
{

WriteResourceManagerState::WriteResourceManagerState()
{
}

bool WriteResourceManagerState::ableToOperate()
{
  if (!this->Superclass::ableToOperate())
  {
    return false;
  }

  // To write the state of the resource manager, we must have access to the
  // manager itself. Also, there must be resources to write.
  smtk::resource::Manager::Ptr resourceManager;
  if (auto managers = this->managers())
  {
    resourceManager = managers->get<smtk::resource::Manager::Ptr>();
  }
  if (!resourceManager || resourceManager->empty())
  {
    return false;
  }

  // Finally, we must have an operation manager from which we access specific
  // write operations.
  if (m_manager.expired())
  {
    return false;
  }

  return true;
}

smtk::operation::Operation::Result WriteResourceManagerState::operateInternal()
{
  // Access the directory into which we write the managed resources.
  auto directoryItem = this->parameters()->findDirectory("directory");

  // Construct the directory if it doesn't already exist.
  smtk::common::Paths::createDirectory(directoryItem->value());

  // Access the resource manager.
  smtk::resource::Manager::Ptr resourceManager;
  if (auto managers = this->managers())
  {
    resourceManager = managers->get<smtk::resource::Manager::Ptr>();
  }
  if (!resourceManager)
  {
    smtkErrorMacro(smtk::io::Logger::instance(), "Cannot resolve resource manager.");
    return this->createResult(smtk::operation::Operation::Outcome::FAILED);
  }

  // Construct a result to populate with resources that have been successfully
  // written.
  auto result = this->createResult(smtk::operation::Operation::Outcome::SUCCEEDED);
  smtk::attribute::ResourceItem::Ptr written = result->findResource("resource");

  // Construct a WriteResource manager. We create the operation using the
  // operation manager because it requires an operation manager to access write
  // methods for specific resource types.
  auto writeResource = m_manager.lock()->create<smtk::operation::WriteResource>();

  writeResource->parameters()->findFile("filename")->setIsEnabled(false);

  // For each resource...
  std::vector<std::shared_ptr<smtk::resource::Resource>> resources;
  resourceManager->visit(
    [&resources](smtk::resource::Resource& rawResource)
    {
      auto resource = rawResource.shared_from_this();
      resources.push_back(resource);
      return smtk::common::Processing::CONTINUE;
    }
  );

  for (auto& resource : resources)
  {
    // ...reset the write operation's associations and file names...
    writeResource->parameters()->associations()->reset();

    // ...and associate the resource to the write operation.
    writeResource->parameters()->associate(resource);

    // Set the resource's write location to the input directory.
    std::string filename = smtk::common::Paths::filename(resource->location());
    if (filename.empty())
    {
      filename = resource->id().toString() + ".smtk";
    }

    // Ensure the resource suffix is .smtk so readers can subsequently read the
    // file.
    if (smtk::common::Paths::extension(resource->location()) != ".smtk")
    {
      filename = smtk::common::Paths::stem(filename) + ".smtk";
    }

    resource->setLocation(directoryItem->value() + "/" + filename);

    // Write the resource to disk. We use writeResource's external operate()
    // method rather than the internal operate({}) method to facilitate resource
    // locking with appropriate permissions.
    auto singleResult = writeResource->operate();

    if (singleResult->findInt("outcome")->value() ==
      static_cast<int>(smtk::operation::Operation::Outcome::SUCCEEDED))
    {
      // Upon success, append the resource to the outcome.
      written->appendValue(resource);
    }
    else
    {
      // If the resource failed to write, change the result status to failure
      // (but continue to write the remaining resources).
      result->findInt("outcome")->setValue(
        static_cast<int>(smtk::operation::Operation::Outcome::FAILED));
    }
  }

  // Once all of the resources have been written, construct an index file
  // describing the resources.
  std::string indexFileName = directoryItem->value() + "/index.txt";
  std::ofstream indexFile(indexFileName.c_str());

  // Iterate over the successfully written resources and add their locations to
  // the index file.
  for (std::size_t i = 0; i < written->numberOfValues(); i++)
  {
    auto resource = written->value(i);
    indexFile << smtk::common::Paths::filename(resource->location()) << "\n";
  }
  indexFile.close();

  // Return the result object.
  return result;
}

const char* WriteResourceManagerState::xmlDescription() const
{
  return WriteResourceManagerState_xml;
}

void WriteResourceManagerState::markModifiedResources(WriteResourceManagerState::Result&)
{
  // We don't need to do anything here. The writers for each resource will have
  // marked the resource as "clean". All we need to do is override the default
  // behavior of marking all resources mentioned in the result as "dirty".
}
}
}
