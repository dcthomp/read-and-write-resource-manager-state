//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#include "pqSMTKReadResourceManagerStateBehavior.h"

// SMTK
#include "smtk/attribute/Attribute.h"
#include "smtk/attribute/DirectoryItem.h"
#include "smtk/extension/paraview/appcomponents/pqSMTKBehavior.h"
#include "smtk/extension/paraview/appcomponents/pqSMTKRenderResourceBehavior.h"
#include "smtk/extension/paraview/appcomponents/pqSMTKResource.h"
#include "smtk/extension/paraview/appcomponents/pqSMTKWrapper.h"
#include "smtk/extension/paraview/server/vtkSMSMTKWrapperProxy.h"
#include "smtk/operation/Manager.h"

#include "operators/ReadResourceManagerState.h"

// Client side
#include "pqActiveObjects.h"
#include "pqApplicationCore.h"
#include "pqCoreUtilities.h"
#include "pqFileDialog.h"
#include "pqObjectBuilder.h"
#include "pqServerManagerModel.h"
#include "vtkSMPropertyHelper.h"
#include "vtkSMProxyDefinitionManager.h"
#include "vtkSMSessionProxyManager.h"

#include <QAction>
#include <QApplication>
#include <QDialog>
#include <QMainWindow>
#include <QMenu>
#include <QMenuBar>
#include <QObject>
#include <QPushButton>
#include <QSharedPointer>

//-----------------------------------------------------------------------------
pqReadResourceManagerStateReaction::pqReadResourceManagerStateReaction(QAction* parentObject)
  : Superclass(parentObject)
{
}

//-----------------------------------------------------------------------------
void pqReadResourceManagerStateReaction::loadResourceManagerState()
{
  // Access the active server
  pqServer* server = pqActiveObjects::instance().activeServer();

  // Construct a file dialog for importing python operations
  pqFileDialog fileDialog(
    server, pqCoreUtilities::mainWidget(), tr("Load All SMTK Resources:"), "", "");
  fileDialog.setObjectName("LoadAllResourcesDialog");
  fileDialog.setFileMode(pqFileDialog::Directory);

  if (fileDialog.exec() == QDialog::Accepted)
  {
    QString fname = fileDialog.getSelectedFiles()[0];

    // Access the server's operation manager (held by pqSMTKWrapper)
    pqSMTKWrapper* wrapper = pqSMTKBehavior::instance()->resourceManagerForServer(server);

    // Construct an operation for saving the resource manager state
    auto readResourceManagerStateOp =
      wrapper->smtkOperationManager()->
      create<smtk::resource_manager_state::ReadResourceManagerState>();

    if (readResourceManagerStateOp == nullptr)
    {
      std::cerr << "Could not create \"read resource manager state\" operation\n";
      return;
    }

    // Set the input python operation file name
    readResourceManagerStateOp->parameters()->findDirectory("directory")->
      setValue(fname.toStdString());
std::cout<<"setting value "<<fname.toStdString()<<std::endl;

    // Execute the operation
    readResourceManagerStateOp->operate();
  }
}

//-----------------------------------------------------------------------------
namespace
{
QAction* findSaveAction(QMenu* menu)
{
  Q_FOREACH (QAction* action, menu->actions())
  {
    if (action->text().contains("save data", Qt::CaseInsensitive))
    {
      return action;
    }
  }
  return NULL;
}

QAction* findHelpMenuAction(QMenuBar* menubar)
{
  QList<QAction*> menuBarActions = menubar->actions();
  Q_FOREACH (QAction* existingMenuAction, menuBarActions)
  {
    QString menuName = existingMenuAction->text().toLower();
    menuName.remove('&');
    if (menuName == "help")
    {
      return existingMenuAction;
    }
  }
  return NULL;
}
}

//-----------------------------------------------------------------------------
static pqSMTKReadResourceManagerStateBehavior* g_instance = nullptr;

pqSMTKReadResourceManagerStateBehavior::pqSMTKReadResourceManagerStateBehavior(QObject* parent)
  : Superclass(parent)
{
  // Wait until the event loop starts, ensuring that the main window will be
  // accessible.
  QTimer::singleShot(
    0, this,
    [this](){
      auto pqCore = pqApplicationCore::instance();
      if (pqCore)
      {
        QAction* readResourceManagerStateAction = new QAction(tr("&Load All Resources..."), this);

        QMainWindow* mainWindow = qobject_cast<QMainWindow*>(pqCoreUtilities::mainWidget());

        if (mainWindow == nullptr)
        {
          return;
        }

        QList<QAction*> menuBarActions = mainWindow->menuBar()->actions();

        QMenu* menu = NULL;
        Q_FOREACH (QAction* existingMenuAction, menuBarActions)
        {
          QString menuName = existingMenuAction->text();
          menuName.remove('&');
          if (menuName == "File")
          {
            menu = existingMenuAction->menu();
            break;
          }
        }

        if (menu)
        {
          // We want to defer the creation of the menu actions as much as possible
          // so the File menu will already be populated by the time we add our
          // custom actions. If our actions are inserted first, there is no way to
          // control where in the list of actions they go, and they end up awkwardly
          // sitting at the top of the menu. By using a single-shot connection to
          // load our actions, we ensure that extant Save methods are in place; we
          // key off of their location to make the menu look better.
          QMetaObject::Connection* connection = new QMetaObject::Connection;
          *connection = QObject::connect(menu, &QMenu::aboutToShow, [=]() {
              QAction* saveAction = findSaveAction(menu);

              menu->insertAction(saveAction, readResourceManagerStateAction);

              // Remove this connection.
              QObject::disconnect(*connection);
              delete connection;
            });
        }
        else
        {
          // If the File menu doesn't already exist, I don't think the following
          // logic works. It is taken from pqPluginActionGroupBehavior, which
          // is designed to accomplish pretty much the same task, though.

          // Create new menu.
          menu = new QMenu("File", mainWindow);
          menu->setObjectName("File");
          menu->addAction(readResourceManagerStateAction);
          // insert new menus before the Help menu is possible.
          mainWindow->menuBar()->insertMenu(::findHelpMenuAction(mainWindow->menuBar()), menu);
        }
        new pqReadResourceManagerStateReaction(readResourceManagerStateAction);
      }
    });
}

pqSMTKReadResourceManagerStateBehavior* pqSMTKReadResourceManagerStateBehavior::instance(QObject* parent)
{
  if (!g_instance)
  {
    g_instance = new pqSMTKReadResourceManagerStateBehavior(parent);
  }

  if (g_instance->parent() == nullptr && parent)
  {
    g_instance->setParent(parent);
  }

  return g_instance;
}

pqSMTKReadResourceManagerStateBehavior::~pqSMTKReadResourceManagerStateBehavior()
{
  if (g_instance == this)
  {
    g_instance = nullptr;
  }

  QObject::disconnect(this);
}
