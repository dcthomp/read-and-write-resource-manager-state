//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#include "pqSMTKWriteResourceManagerStateBehavior.h"

// SMTK
#include "smtk/attribute/Attribute.h"
#include "smtk/attribute/DirectoryItem.h"
#include "smtk/extension/paraview/appcomponents/pqSMTKBehavior.h"
#include "smtk/extension/paraview/appcomponents/pqSMTKRenderResourceBehavior.h"
#include "smtk/extension/paraview/appcomponents/pqSMTKResource.h"
#include "smtk/extension/paraview/appcomponents/pqSMTKWrapper.h"
#include "smtk/extension/paraview/server/vtkSMSMTKWrapperProxy.h"
#include "smtk/operation/Manager.h"

#include "operators/WriteResourceManagerState.h"

// Client side
#include "pqActiveObjects.h"
#include "pqApplicationCore.h"
#include "pqCoreUtilities.h"
#include "pqFileDialog.h"
#include "pqObjectBuilder.h"
#include "pqServerManagerModel.h"
#include "vtkSMPropertyHelper.h"
#include "vtkSMProxyDefinitionManager.h"
#include "vtkSMSessionProxyManager.h"

#include <QAction>
#include <QApplication>
#include <QDialog>
#include <QMainWindow>
#include <QMenu>
#include <QMenuBar>
#include <QObject>
#include <QPushButton>
#include <QSharedPointer>

//-----------------------------------------------------------------------------
pqWriteResourceManagerStateReaction::pqWriteResourceManagerStateReaction(QAction* parentObject)
  : Superclass(parentObject)
{
}

//-----------------------------------------------------------------------------
void pqWriteResourceManagerStateReaction::saveResourceManagerState()
{
  // Access the active server
  pqServer* server = pqActiveObjects::instance().activeServer();

  // Construct a file dialog for importing python operations
  pqFileDialog fileDialog(
    server, pqCoreUtilities::mainWidget(), tr("Save All SMTK Resources:"), "", "");
  fileDialog.setObjectName("SaveAllResourcesDialog");
  fileDialog.setFileMode(pqFileDialog::AnyFile);

  if (fileDialog.exec() == QDialog::Accepted)
  {
    QString fname = fileDialog.getSelectedFiles()[0];

    // Access the server's operation manager (held by pqSMTKWrapper)
    pqSMTKWrapper* wrapper = pqSMTKBehavior::instance()->resourceManagerForServer(server);

    // Construct an operation for saving the resource manager state
    auto writeResourceManagerStateOp =
      wrapper->smtkOperationManager()->
      create<smtk::resource_manager_state::WriteResourceManagerState>();

    if (writeResourceManagerStateOp == nullptr)
    {
      std::cerr << "Could not create \"write resource manager state\" operation\n";
      return;
    }

    // Set the input python operation file name
    writeResourceManagerStateOp->parameters()->findDirectory("directory")->
      setValue(fname.toStdString());

    // Execute the operation
    writeResourceManagerStateOp->operate();
  }
}

//-----------------------------------------------------------------------------
namespace
{
QAction* findSaveAction(QMenu* menu)
{
  Q_FOREACH (QAction* action, menu->actions())
  {
    if (action->text().contains("save data", Qt::CaseInsensitive))
    {
      return action;
    }
  }
  return NULL;
}

QAction* findHelpMenuAction(QMenuBar* menubar)
{
  QList<QAction*> menuBarActions = menubar->actions();
  Q_FOREACH (QAction* existingMenuAction, menuBarActions)
  {
    QString menuName = existingMenuAction->text().toLower();
    menuName.remove('&');
    if (menuName == "help")
    {
      return existingMenuAction;
    }
  }
  return NULL;
}
}

//-----------------------------------------------------------------------------
static pqSMTKWriteResourceManagerStateBehavior* g_instance = nullptr;

pqSMTKWriteResourceManagerStateBehavior::pqSMTKWriteResourceManagerStateBehavior(QObject* parent)
  : Superclass(parent)
{
  // Wait until the event loop starts, ensuring that the main window will be
  // accessible.
  QTimer::singleShot(
    0, this,
    [this](){
      auto pqCore = pqApplicationCore::instance();
      if (pqCore)
      {
        QAction* writeResourceManagerStateAction =
          new QAction(tr("&Save All Resources As..."), this);

        QMainWindow* mainWindow = qobject_cast<QMainWindow*>(pqCoreUtilities::mainWidget());

        if (mainWindow == nullptr)
        {
          return;
        }

        QList<QAction*> menuBarActions = mainWindow->menuBar()->actions();

        QMenu* menu = NULL;
        Q_FOREACH (QAction* existingMenuAction, menuBarActions)
        {
          QString menuName = existingMenuAction->text();
          menuName.remove('&');
          if (menuName == "File")
          {
            menu = existingMenuAction->menu();
            break;
          }
        }

        if (menu)
        {
          // We want to defer the creation of the menu actions as much as possible
          // so the File menu will already be populated by the time we add our
          // custom actions. If our actions are inserted first, there is no way to
          // control where in the list of actions they go, and they end up awkwardly
          // sitting at the top of the menu. By using a single-shot connection to
          // load our actions, we ensure that extant Save methods are in place; we
          // key off of their location to make the menu look better.
          QMetaObject::Connection* connection = new QMetaObject::Connection;
          *connection = QObject::connect(menu, &QMenu::aboutToShow, [=]() {
              QAction* saveAction = findSaveAction(menu);

              menu->insertAction(saveAction, writeResourceManagerStateAction);

              // Remove this connection.
              QObject::disconnect(*connection);
              delete connection;
            });
        }
        else
        {
          // If the File menu doesn't already exist, I don't think the following
          // logic works. It is taken from pqPluginActionGroupBehavior, which
          // is designed to accomplish pretty much the same task, though.

          // Create new menu.
          menu = new QMenu("File", mainWindow);
          menu->setObjectName("File");
          menu->addAction(writeResourceManagerStateAction);
          // insert new menus before the Help menu is possible.
          mainWindow->menuBar()->insertMenu(::findHelpMenuAction(mainWindow->menuBar()), menu);
        }
        new pqWriteResourceManagerStateReaction(writeResourceManagerStateAction);
      }
    });
}

pqSMTKWriteResourceManagerStateBehavior* pqSMTKWriteResourceManagerStateBehavior::instance(QObject* parent)
{
  if (!g_instance)
  {
    g_instance = new pqSMTKWriteResourceManagerStateBehavior(parent);
  }

  if (g_instance->parent() == nullptr && parent)
  {
    g_instance->setParent(parent);
  }

  return g_instance;
}

pqSMTKWriteResourceManagerStateBehavior::~pqSMTKWriteResourceManagerStateBehavior()
{
  if (g_instance == this)
  {
    g_instance = nullptr;
  }

  QObject::disconnect(this);
}
